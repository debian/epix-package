/* -*-ePiX-*- */
#include "epix.h"
using namespace ePiX;

/*
 * Line and fill style test; draws all 32 combinations of five attributes:
 *
 * line color: (Neutral: cols 1, 3; Red, cols 2, 4)
 * line style: (Solid: cols 1, 2; dashed: cols 3, 4)
 *
 * base color: (Neutral: odd rows, Blue otherwise)
 * base width: (1pt: rows 1-2, 5-6, 4pt otherwise)
 *
 * fill color: (Neutral: rows 1-4, Yellow otherwise)
 */

// test objects
void objs()
{
  rect(P(0,0), P(1,1));
  line(P(-1,-1), P(1,-1));
}

// attribute-setting commands on bool flags
void line_color(bool arg)
{
  if (arg)
    pen(Red(), 2);

  else
    pen(Neutral(), 2);
}

void line_style(bool arg)
{
  if (arg)
    dashed();
}

void base_color(bool col, double wid)
{
  if (col)
    base(Blue(1.2), wid);

  else
    base(Neutral(), wid);
}

void base_pen(bool col, bool wid)
{
  if (wid)
    base_color(col, 4);

  else
    base_color(col, 1);
}

void fill_color(bool arg)
{
  if (arg)
    fill(Yellow());

  else
    fill(Neutral());
}

// we'll use 0, 1 as loop indices; convert to bool
bool tf(int i)
{
  return i == 0 ? false : true;
}

// where to position the result of a test
P loc(int i0, int i1, int i2, int i3, int i4)
{
  double horiz(0), vert(7);
  if (tf(i0))
    horiz += 1;

  if (tf(i1))
    horiz += 2;

  if (tf(i2))
    vert -= 1;

  if(tf(i3))
    vert -= 2;

  if (tf(i4))
    vert -= 4;

  return P(horiz, vert);
}

int main()
{
  picture(P(0,0), P(4,8), "6 x 9in");

  begin();

  // the tests proper
  for (int i0=0; i0<2; ++i0)
    for (int i1=0; i1<2; ++i1)
      for (int i2=0; i2<2; ++i2)
	for (int i3=0; i3<2; ++i3)
	  for (int i4=0; i4<2; ++i4)
	    {
	      screen scr(P(-1,-1), P(1,1));
	      activate(scr);

	      solid(); // may need to reset line style
	      border(Green(0.6), "0.1pt");

	      line_color(tf(i0));
	      line_style(tf(i1));
	      base_pen(tf(i2),tf(i3));
	      fill_color(tf(i4));

	      objs();

	      scr.scale(0.9);
	      inset(loc(i0,i1,i2,i3,i4), loc(i0,i1,i2,i3,i4) + P(1,1));
	      deactivate(scr);
	    }
  end();
}
