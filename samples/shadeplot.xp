/* -*-ePiX-*- */
#include "epix.h"
using namespace ePiX;
double k(4);         // change width of hump
double dx(0.05);     // width of thin shaded region
double x(1/sqrt(k)); // position of thin shaded region

const double dy(0.5);

double f(double t)
{
  return sqrt(fabs(k)/(2*M_PI))*exp(-k*t*t);
}

P pt1(x, f(x)+2*dy);
P pt2(x+dx,f(x)+dy);
P pt3(x+3*dx,f(x));

Color shade1(Black(0.1));
Color shade2(Black(0.4));
Color shade3(Black(0.6));

int main()
{
  picture(P(0,0), P(1,1), "150x150pt");

  begin();
  legend L;
  L.backing(Neutral());

  fill(shade1); 
  shadeplot(f, xmin(), x, 90);
  L.fill_item("$=\\displaystyle\\int_a^x f(t)\\,dt$");

  fill(shade2);
  rect(P(x,0), P(x+dx, f(x)));
  L.fill_item("= $f(x)\\,dx\\vphantom{\\Bigg|}$");

  fill(shade3);
  shadeplot(f, x, x+dx, 10);
  L.fill_item("= $F(x+dx)-F(x)$");

  bold();
  plot(f, xmin(), xmax(), 120);

  plain();
  h_axis(4);
  v_axis(4);

  font_size("scriptsize");
  L.draw(canvas().tr(), P(-2,-2), bl);

  font_size("footnotesize");
  label(P(xmin(),0), P(0,-5), "$a$", b);
  label(P(x,0), P(0,-5), "$x$", b);
  label(P(x+dx,0), P(0,-2), "$x+dx$", br);

  pst_format();

  end();
}
