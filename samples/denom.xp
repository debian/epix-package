/* -*-ePiX-*- */
#include "epix.h"
using namespace ePiX;

int N(30); // maximum denominator plotted

int main() 
{
  picture(P(-2,0), P(2,1), "4x1in");

  begin();   
  tikz_format();

  axis Ax(P(-2,0), P(2,0), 8, P(0,-6), b);
  Ax.subdivide(6).frac().draw();

  v_axis(2);

  dot_size(2);
  for (int i=1; i< N; ++i) 
    for (int j=i*xmin(); j <= i*xmax(); ++j) 
      if (gcd(i, j) == 1) 
        ddot(P(j*1.0/i, 1.0/i));

  font_size("scriptsize");

  // The long label text can be written in one string, but using a
  // stringstream allows the input to be formatted more readably.
  std::stringstream buf;
  buf << "$f(x)=\\begin{cases}%" << std::endl
      << "  \\frac{1}{q} & "
      << "\\text{ $x=\\frac{p}{q}$ in lowest terms} \\\\" << std::endl
      << "  0 & \\text{ $x$ irrational}%" << std::endl
      << "\\end{cases}$";

  label(P(0,0.75), P(2,0), buf.str(), r);
  end();
}

