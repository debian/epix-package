/* -*-ePiX-*- */
#include "epix.h"
using namespace ePiX;

// compile with, e.g., elaps -DALL polyhedra.xp

const double MAX(1.25);

#define A5

// draw everyone
#ifdef ALL
#define A4
#define A5
#endif /* ALL */

// draw three polyhedra with A4 symmetry
#ifdef A4
#define COLOR
#define TETRA
#define CUBE
#define OCTA
#endif /* A4 */

// draw two polyhedra with A5 symmetry
#ifdef A5
#ifndef COLOR
#define COLOR
#endif /* ndef COLOR */
#define DODECA
#define ICOSA
#endif /* A5 */

int main()
{
  picture(P(-MAX,-MAX),P(MAX,MAX), "6 x 6in");

  begin();
  const double gam(0.5*(1+sqrt(5)));

  grid();
  camera.at(P(8,3,2));
  camera.range(20);

  Sphere S;
  frame f;
  frame f2(E_2, E_1, E_3);

#ifdef TETRA
#ifdef COLOR
  rgb(1,0.7,0.7);
#endif /* COLOR */
  back_tetra(S, f);
  back_tetra(S, f2);
#endif

#ifdef CUBE
#ifdef COLOR
  rgb(0.7,0.7,1);
#endif /* COLOR */
  back_cube(S, f);
#endif

#ifdef OCTA
#ifdef COLOR
  rgb(0.7,1,0.7);
#endif /* COLOR */
  back_octa(S, f);
#endif

#ifdef DODECA
#ifdef COLOR
  rgb(0.7,1,1);
#endif /* COLOR */
  back_dodeca(S, f);
#endif

#ifdef ICOSA
#ifdef COLOR
  rgb(1,1,0.7);
#endif /* COLOR */
  back_icosa(S, f);
#endif

  bold();

#ifdef TETRA
#ifdef COLOR
  red();
#endif /* COLOR */
  front_tetra(S, f);
  front_tetra(S, f2);
#endif

#ifdef CUBE
#ifdef COLOR
  blue();
#endif /* COLOR */
  front_cube(S, f);
#endif

#ifdef OCTA
#ifdef COLOR
  green(0.6);
#endif /* COLOR */
  front_octa(S, f);
#endif

#ifdef DODECA
#ifdef COLOR
  rgb(0, 0.7, 0.7);
#endif /* COLOR */
  front_dodeca(S, f);
#endif

#ifdef ICOSA
#ifdef COLOR
  rgb(1, 0.9, 0);
#endif /* COLOR */
  front_icosa(S, f);
#endif

#ifdef COLOR
  green(0.8);
#endif
  S.draw();

  end();
}
