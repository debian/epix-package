;; -*-emacs-lisp-*-
;;
;; Emacs startup file for the Debian epix package
;;
;; Originally contributed by Nils Naumann <naumann@unileoben.ac.at>
;; Modified by Dirk Eddelbuettel <edd@debian.org>
;; Adapted for dh-make by Jim Van Zandt <jrv@vanzandt.mv.com>
;; Adapted for dh-elpa by Julian Gilbey <jdg@debian.org>

;;;###autoload
(add-to-list 'auto-mode-alist '("\\.xp\\'" . epix-mode))
;;;###autoload
(autoload 'epix-mode "epix" "ePiX editing mode" t)
;;;###autoload
(setq epix-mark-files-as-epix t)
;;;###autoload
(setq epix-insert-template-in-empty-buffer t)

;;;###autoload
(autoload 'flix-mode "epix" "ePiX editing mode" t)
;;;###autoload
(add-to-list 'auto-mode-alist '("\\.flx\\'" . flix-mode))
